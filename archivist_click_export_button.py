#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to click the "Export" button
        - from https://closer-archivist.herokuapp.com/admin/instruments/exports
"""

from selenium.webdriver.common.by import By
import pandas as pd
import time
import sys
import os

from mylib import get_driver, url_base, archivist_login_all, get_base_url


driver = get_driver()


def click_export_button(df, uname, pw):
    """
    Loop over xml_name dictionary, click 'Export'
    """
    df_base_url = get_base_url(df)
    export_name = pd.Series(df_base_url.base_url.values, index=df.Instrument).to_dict()
    print("Got {} xml names".format(len(export_name)))
    print(df)
    print(export_name)

    ok = archivist_login_all(driver, export_name.values(), uname, pw)

    k = 0
    for prefix, url in export_name.items():
        if url:
            base = url_base(url)
            print('Working on item "{}" from "{}" with URL "{}"'.format(prefix, base, url))
            if not ok[base]:
                print("host {} was not available, skipping".format(base))
                continue
            driver.get(url)
            time.sleep(10)

            print(k)
            # find the input box
            inputElement = driver.find_element(by=By.XPATH, value='//input[@placeholder="Search by prefix (press return to perform search)"]')
            print(inputElement)
            print("sending keys")
            inputElement.send_keys(prefix)
            print("done sending keys")
            # locate id and link
            #TODO: make this better
            trs = driver.find_elements(by=By.XPATH, value="html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")
            print(trs)

            for i, tr in enumerate(trs):
                print(i)

                # column 2 is "Prefix"
                xml_prefix = tr.find_elements(by=By.XPATH, value="td")[1].text
                print(xml_prefix)

                # column 5 is "Actions", click on "Create new export"
                #TODO: this is case sensitive
                exportButton = tr.find_element(By.LINK_TEXT, "CREATE NEW EXPORT")
                print(exportButton)
                if (xml_prefix == prefix):
                    print("Click export button for " + prefix)
                    exportButton.click()
                    time.sleep(5)
                else:
                    print("Did not find " + prefix)
    driver.quit()


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]
    # prefixes
    df = pd.read_csv('Prefixes_to_export.txt', sep='\t')

    click_export_button(df, uname, pw)


if __name__ == "__main__":
    main()

